USE [Titles]
GO

/****** Object:  StoredProcedure [dbo].[GetTitleDetails]    Script Date: 6/9/2020 10:20:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetTitleDetails]
 @titleId int
AS
BEGIN
	-- Get Basic Title Information
	SELECT t.TitleId, t.TitleName, t.TitleNameSortable, t.ReleaseYear, t.ProcessedDateTimeUTC
	FROM Title t
	WHERE t.TitleId = @titleId

	-- Get Awards
	SELECT a.Id, a.Award as AwardTitle, a.AwardCompany, a.AwardWon, a.AwardYear
	FROM Award a
	WHERE a.TitleId = @titleId

	-- Get StoryLines
	SELECT s.Id, s.Description, s.Language, s.Type
	FROM StoryLine s
	WHERE s.TitleId = @titleId

	-- Get Other Names
	SELECT o.Id, o.TitleNameLanguage, o.TitleNameType, o.TitleNameSortable, o.TitleName
	FROM OtherName o
	WHERE o.TitleId = @titleId

	-- Get Genres
	SELECT tg.GenreId, g.Name
	FROM TitleGenre tg
	JOIN Genre g on tg.GenreId = g.Id
	WHERE tg.TitleId = @titleId

	-- Get Participants
	SELECT tp.ParticipantId, tp.IsKey, tp.RoleType, tp.IsOnScreen, p.Name, p.ParticipantType
	FROM TitleParticipant tp
	JOIN Participant p on tp.ParticipantId = p.Id
	WHERE tp.TitleId = @titleId
END
GO

