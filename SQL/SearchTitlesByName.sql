USE [Titles]
GO

/****** Object:  StoredProcedure [dbo].[SearchTitlesByName]    Script Date: 6/9/2020 10:21:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SearchTitlesByName]
 @titleName nvarchar(100) = ''
AS
BEGIN
	SELECT TitleId, TitleName, TitleNameSortable, ReleaseYear, ProcessedDateTimeUTC
	FROM Title
	WHERE TitleName LIKE '%' + @titleName + '%'
END
GO

