﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

export interface TitlesState {
    isLoading: boolean;
    name?: string;
    titles: Title[];
}

export interface Title {
    titleId: number;
    titleName: string;
    titleNameSortable: string;
    releaseYear: number;
    processedDateTimeUTC: string;
}

interface RequestTitlesAction {
    type: 'REQUEST_TITLES';
    name?: string;
}

interface ReceiveTitlesAction {
    type: 'RECEIVE_TITLES';
    name?: string;
    titles: Title[];
}

type KnownAction = RequestTitlesAction | ReceiveTitlesAction;

export const actionCreators = {
    requestTitles: (name: string): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.titles) {
            let searchUri = `api/titles/search`;
            if (name) {
                searchUri = `api/titles/search?name=${name}`;
            }
            fetch(searchUri)
                .then(response => response.json() as Promise<Title[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_TITLES', name: name, titles: data });
                });

            dispatch({ type: 'REQUEST_TITLES', name: name });
        }
    }
};

const unloadedState: TitlesState = { titles: [], isLoading: false };

export const reducer: Reducer<TitlesState> = (state: TitlesState | undefined, incomingAction: Action): TitlesState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_TITLES':
            return {
                name: action.name,
                titles: state.titles,
                isLoading: true
            };
        case 'RECEIVE_TITLES':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.name === state.name) {
                return {
                    name: action.name,
                    titles: action.titles,
                    isLoading: false
                };
            }
            break;
    }

    return state;
}
