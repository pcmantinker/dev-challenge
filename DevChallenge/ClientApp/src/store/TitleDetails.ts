﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import { Title } from './Titles';

export interface TitleDetailsState {
    isLoading: boolean;
    titleId: number;
    titleDetails: TitleDetails;
}

export interface Participant {
    id: number,
    name: string,
    participantType: string,
    isKey: boolean,
    roleType: string,
    isOnScreen: boolean
}

export interface Genre {
    id: number,
    name: string
}

export interface StoryLine {
    id: number,
    type: string,
    language: string,
    description: string
}

export interface OtherName {
    id: number,
    titleNameLanguage: string,
    titleNameType: string,
    titleNameSortable: string,
    titleName: string
}

export interface Award {
    id: number,
    awardWon: boolean,
    awardYear: number,
    awardTitle: string,
    awardCompany: string
}

export interface TitleDetails extends Title {
    participants: Participant[];
    genres: Genre[];
    storyLines: StoryLine[];
    otherNames: OtherName[];
    awards: Award[];
}

interface RequestTitleDetailsAction {
    type: 'REQUEST_TITLE_DETAILS';
    titleId: number;
}

interface ReceiveTitleDetailsAction {
    type: 'RECEIVE_TITLE_DETAILS';
    titleId: number;
    titleDetails: TitleDetails;
}

type KnownAction = RequestTitleDetailsAction | ReceiveTitleDetailsAction;

export const actionCreators = {
    requestTitleDetails: (titleId: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();
        if (appState && appState.titles) {
            let searchUri = `api/titles/details/${titleId}`;
            fetch(searchUri)
                .then(response => response.json() as Promise<TitleDetails>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_TITLE_DETAILS', titleId: titleId, titleDetails: data });
                });

            dispatch({ type: 'REQUEST_TITLE_DETAILS', titleId: titleId });
        }
    }
};

const unloadedState: TitleDetailsState = {
    titleDetails: {
        participants: [],
        genres: [],
        storyLines: [],
        otherNames: [],
        awards: [],
        titleId: 0,
        titleName: '',
        titleNameSortable: '',
        releaseYear: 0,
        processedDateTimeUTC: ''

    }, titleId: 0, isLoading: false
};

export const reducer: Reducer<TitleDetailsState> = (state: TitleDetailsState | undefined, incomingAction: Action): TitleDetailsState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_TITLE_DETAILS':
            return {
                titleId: action.titleId,
                titleDetails: state.titleDetails,
                isLoading: true
            };
        case 'RECEIVE_TITLE_DETAILS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.titleId === state.titleId) {
                return {
                    titleId: action.titleId,
                    titleDetails: action.titleDetails,
                    isLoading: false
                };
            }
            break;
    }

    return state;
}
