﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as TitleDetailsStore from '../store/TitleDetails';

// At runtime, Redux will merge together...
type TitleDetailsProps =
    TitleDetailsStore.TitleDetailsState // ... state we've requested from the Redux store
    & typeof TitleDetailsStore.actionCreators // ... plus action creators we've requested
    & RouteComponentProps<{ titleId: string }>; // ... plus incoming routing parameters

class TitleDetails extends React.PureComponent<TitleDetailsProps> {
    // This method is called when the component is first added to the document
    public componentDidMount() {
        this.fetchData();
    }

    public render() {
        return (
            <React.Fragment>
                <h1 id="tabelLabel">Title Details for {this.props.titleDetails.titleName} ({this.props.titleDetails.releaseYear})</h1>
                <h2>Story Lines</h2>
                {this.renderStoryLines()}
                <h2>Awards</h2>
                {this.renderAwards()}
                <h2>Genres</h2>
                {this.renderGenres()}
                <h2>Other Names</h2>
                {this.renderOtherNames()}
                <h2>Participants</h2>
                {this.renderParticipants()}
            </React.Fragment>
        );
    }

    private fetchData() {
        const titleId = parseInt(this.props.match.params.titleId, 10) || 0;
        this.props.requestTitleDetails(titleId);
    }

    private renderStoryLines() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Language</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.titleDetails.storyLines.map((storyLine: TitleDetailsStore.StoryLine) =>
                        <tr key={storyLine.id}>
                            <td>{storyLine.description}</td>
                            <td>{storyLine.language}</td>
                            <td>{storyLine.type}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    private renderAwards() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Award</th>
                        <th>Award Company</th>
                        <th>Award Won</th>
                        <th>Award Year</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.titleDetails.awards.map((award: TitleDetailsStore.Award) =>
                        <tr key={award.id}>
                            <td>{award.awardTitle}</td>
                            <td>{award.awardCompany}</td>
                            <td>{award.awardWon}</td>
                            <td>{award.awardYear}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    private renderOtherNames() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Language</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.titleDetails.otherNames.map((otherName: TitleDetailsStore.OtherName) =>
                        <tr key={otherName.id}>
                            <td>{otherName.titleName}</td>
                            <td>{otherName.titleNameLanguage}</td>
                            <td>{otherName.titleNameType}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    private renderGenres() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.titleDetails.genres.map((genre: TitleDetailsStore.Genre) =>
                        <tr key={genre.id}>
                            <td>{genre.name}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    private renderParticipants() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Is Key</th>
                        <th>Is On Screen</th>
                        <th>Participant Type</th>
                        <th>Role Type</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.titleDetails.participants.map((participant: TitleDetailsStore.Participant) =>
                        <tr key={participant.id}>
                            <td>{participant.name}</td>
                            <td>{participant.isKey}</td>
                            <td>{participant.isOnScreen}</td>
                            <td>{participant.participantType}</td>
                            <td>{participant.roleType}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.titleDetails, // Selects which state properties are merged into the component's props
    TitleDetailsStore.actionCreators // Selects which action creators are merged into the component's props
)(TitleDetails as any);