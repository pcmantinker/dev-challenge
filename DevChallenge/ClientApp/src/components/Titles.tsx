﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../store';
import * as TitlesStore from '../store/Titles';

// At runtime, Redux will merge together...
type TitlesProps =
    TitlesStore.TitlesState // ... state we've requested from the Redux store
    & typeof TitlesStore.actionCreators // ... plus action creators we've requested
    & RouteComponentProps<{ name: string }>; // ... plus incoming routing parameters

class Titles extends React.PureComponent<TitlesProps> {
    input: React.RefObject<HTMLInputElement>;

    constructor(props: Readonly<TitlesProps>) {
        super(props);
        this.input = React.createRef();
        this.fetchData = this.fetchData.bind(this);
        this.keyPressed = this.keyPressed.bind(this);
    }

    // This method is called when the component is first added to the document
    public componentDidMount() {
        this.fetchData();
    }

    public render() {
        return (
            <React.Fragment>
                <h1 id="tabelLabel">Titles</h1>
                <div className="input-group mb-3">
                    <input type="text" className="form-control" placeholder="Title Name" aria-label="Title Name" aria-describedby="basic-addon2" ref={this.input} onKeyPress={this.keyPressed} />
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button" onClick={this.fetchData}> Search</button>
                    </div>
                </div>
                {this.renderTitlesTable()}
            </React.Fragment>
        );
    }

    private fetchData() {
        let name = '';
        if (this.input.current) {
            name = this.input.current.value;
        }
        this.props.requestTitles(name);
    }

    private keyPressed(event: React.KeyboardEvent) {
        if (event.key === "Enter") {
            this.fetchData()
        }
    }

    private renderTitlesTable() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Release Year</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.titles.map((title: TitlesStore.Title) =>
                        <tr key={title.titleId} className="pointer" onClick={e => { this.props.history.push(`/details/${title.titleId}`) }}>
                            <td>{title.titleName}</td>
                            <td>{title.releaseYear}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.titles, // Selects which state properties are merged into the component's props
    TitlesStore.actionCreators // Selects which action creators are merged into the component's props
)(Titles as any);

