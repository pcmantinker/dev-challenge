import * as React from 'react';
import { connect } from 'react-redux';

const Home = () => (
    <div>
        <h1>Development Challenge</h1>
        <p>This application was written in .NET Core 3.1, React, and React Redux 7.2. The datastore is hosted on a SQL Server 2019 Docker instance.</p>
        <p>Click on <a href="/titles"><b>Title Search</b></a> in the navigation menu to begin.</p>
    </div>
);

export default connect()(Home);
