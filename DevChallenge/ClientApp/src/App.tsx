import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Counter from './components/Counter';
import FetchData from './components/FetchData';
import Titles from './components/Titles';
import TitleDetails from './components/TitleDetails';
import './custom.css'

export default () => (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/titles' component={Titles} />
        <Route path='/details/:titleId' component={TitleDetails} />
    </Layout>
);
