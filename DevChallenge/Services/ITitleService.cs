﻿using DevChallenge.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DevChallenge.Services
{
  public interface ITitleService
  {
    Task<IEnumerable<Title>> SearchTitlesByName(string name);
    Task<TitleDetails> GetTitleDetails(int titleId);
  }
}
