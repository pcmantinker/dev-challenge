﻿using DevChallenge.Services.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DevChallenge.Services
{
  public class TitleService : ITitleService
  {
    private readonly string _connectionString;

    public TitleService(IConfiguration configuration)
    {
      _connectionString = configuration.GetConnectionString("DefaultConnection");
    }

    public async Task<TitleDetails> GetTitleDetails(int titleId)
    {
      using (var connection = new SqlConnection(_connectionString))
      {
        TitleDetails details = new TitleDetails(titleId);

        await connection.OpenAsync();

        using (var command = connection.CreateCommand())
        {
          command.CommandText = "GetTitleDetails";
          command.CommandType = System.Data.CommandType.StoredProcedure;
          command.Parameters.AddWithValue("titleId", titleId);

          using (var reader = command.ExecuteReader())
          {
            // Read Title Base Information
            while (await reader.ReadAsync())
            {
              details.TitleName = Convert.ToString(reader["TitleName"]);
              details.TitleNameSortable = Convert.ToString(reader["TitleNameSortable"]);
              details.ReleaseYear = Convert.ToInt32(reader["ReleaseYear"]);
              details.ProcessedDateTimeUTC = Convert.ToDateTime(reader["ProcessedDateTimeUTC"]);
            }

            await reader.NextResultAsync();

            // Read Awards
            while (await reader.ReadAsync())
            {
              var award = new Award
              {
                Id = Convert.ToInt32(reader["Id"]),
                AwardCompany = Convert.ToString(reader["AwardCompany"]),
                AwardTitle = Convert.ToString(reader["AwardTitle"]),
                AwardWon = Convert.ToBoolean(reader["AwardWon"]),
                AwardYear = Convert.ToInt32(reader["AwardYear"])
              };

              details.Awards.Add(award);
            }

            await reader.NextResultAsync();

            // Read Story Lines
            while(await reader.ReadAsync())
            {
              var storyLine = new StoryLine
              {
                 Id = Convert.ToInt32(reader["Id"]),
                 Description = Convert.ToString(reader["Description"]),
                 Language = Convert.ToString(reader["Language"]),
                 Type = Convert.ToString(reader["Type"])
              };

              details.StoryLines.Add(storyLine);
            }

            await reader.NextResultAsync();

            // Read Other Names
            while (await reader.ReadAsync())
            {
              var otherName = new OtherName
              {
                Id = Convert.ToInt32(reader["Id"]),
                TitleName = Convert.ToString(reader["TitleName"]),
                TitleNameSortable = Convert.ToString(reader["TitleNameSortable"]),
                TitleNameType = Convert.ToString(reader["TitleNameType"]),
                TitleNameLanguage = Convert.ToString(reader["TitleNameLanguage"])
              };

              details.OtherNames.Add(otherName);
            }

            await reader.NextResultAsync();

            // Read Genres
            while (await reader.ReadAsync())
            {
              var genre = new Genre
              {
                Id = Convert.ToInt32(reader["GenreId"]),
                Name = Convert.ToString(reader["Name"])
              };

              details.Genres.Add(genre);
            }

            await reader.NextResultAsync();

            // Read Participants
            while (await reader.ReadAsync())
            {
              var participant = new Participant
              {
                Id = Convert.ToInt32(reader["ParticipantId"]),
                Name = Convert.ToString(reader["Name"]),
                IsKey = Convert.ToBoolean(reader["IsKey"]),
                IsOnScreen = Convert.ToBoolean(reader["IsOnScreen"]),
                ParticipantType = Convert.ToString(reader["ParticipantType"]),
                RoleType = Convert.ToString(reader["RoleType"])
              };

              details.Participants.Add(participant);
            }
          }
        }
        return details;
      }
    }

    public async Task<IEnumerable<Title>> SearchTitlesByName(string name)
    {
      using (var connection = new SqlConnection(_connectionString))
      {
        List<Title> titles = new List<Title>();

        await connection.OpenAsync();

        using (var command = connection.CreateCommand())
        {
          command.CommandText = "SearchTitlesByName";
          command.CommandType = System.Data.CommandType.StoredProcedure;
          command.Parameters.AddWithValue("titleName", name);

          using (var reader = command.ExecuteReader())
          {
            while (await reader.ReadAsync())
            {
              var title = new Title
              {
                TitleId = Convert.ToInt32(reader["TitleId"]),
                TitleName = Convert.ToString(reader["TitleName"]),
                TitleNameSortable = Convert.ToString(reader["TitleNameSortable"]),
                ReleaseYear = Convert.ToInt32(reader["ReleaseYear"]),
                ProcessedDateTimeUTC = Convert.ToDateTime(reader["ProcessedDateTimeUTC"])
              };

              titles.Add(title);
            }
          }
        }

        return titles;
      }
    }
  }
}
