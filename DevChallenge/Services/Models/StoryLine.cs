﻿
namespace DevChallenge.Services.Models
{
  public class StoryLine
  {
    public int Id { get; set; }
    public string Type { get; set; }
    public string Language { get; set; }
    public string Description { get; set; }
  }
}
