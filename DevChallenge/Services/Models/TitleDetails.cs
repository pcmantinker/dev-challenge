﻿using System.Collections.Generic;

namespace DevChallenge.Services.Models
{
  public class TitleDetails : Title
  {   
    public List<Participant> Participants { get; set; }
    public List<Genre> Genres { get; set; }
    public List<StoryLine> StoryLines { get; set; }
    public List<OtherName> OtherNames { get; set; }
    public List<Award> Awards { get; set; }

    public TitleDetails(int titleId)
    {
      TitleId = titleId;
      Participants = new List<Participant>();
      Genres = new List<Genre>();
      StoryLines = new List<StoryLine>();
      OtherNames = new List<OtherName>();
      Awards = new List<Award>();
    }
  }
}
