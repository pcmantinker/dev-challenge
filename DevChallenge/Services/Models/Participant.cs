﻿
namespace DevChallenge.Services.Models
{
  public class Participant
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string ParticipantType { get; set; }
    public bool IsKey { get; set; }
    public string RoleType { get; set; }
    public bool IsOnScreen { get; set; }
  }
}
