﻿
namespace DevChallenge.Services.Models
{
  public class Award
  {
    public int Id { get; set; }
    public bool AwardWon { get; set; }
    public int AwardYear { get; set; }
    public string AwardTitle { get; set; }
    public string AwardCompany { get; set; }
  }
}
