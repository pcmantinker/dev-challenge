﻿using DevChallenge.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DevChallenge.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class TitlesController : ControllerBase
  {
    private readonly ITitleService _titleService;

    public TitlesController(ITitleService titleService)
    {
      _titleService = titleService;
    }

    public IActionResult Get()
    {
      return Ok("Titles");
    }

    [HttpGet]
    [Route("search")]
    public async Task<IActionResult> SearchTitlesByName(string name)
    {
      try
      {
        var titles = await _titleService.SearchTitlesByName(name);
        return Ok(titles);
      }
      catch(Exception ex)
      {
        return BadRequest(ex.Message);
      }
    }

    [HttpGet]
    [Route("details/{titleId}")]
    public async Task<IActionResult> GetTitleDetails(int titleId)
    {
      try
      {
        var details = await _titleService.GetTitleDetails(titleId);
        return Ok(details);
      }
      catch(Exception ex)
      {
        return BadRequest(ex.Message);
      }
    }
  }
}
